export class CreateDtoProduct {
   readonly title: string
   readonly price: number
}