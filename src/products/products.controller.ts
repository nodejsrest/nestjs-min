import { Body, Controller, Delete, Get, Header, HttpCode, HttpStatus, Param, Post, Put, Redirect, Req, Res } from '@nestjs/common';
import { Request, Response } from 'express';
import { CreateDtoProduct } from './dto/create-product.dto';
import { ProductsService } from './products.service';

@Controller('products')
export class ProductsController {
   constructor(private readonly productService: ProductsService) {

   }

   @Get()
   // @Redirect('https://google.com', 301)

   /* getAll(@Req() req: Request, @Res() res: Response): string {
      res.status(200).end('OK')
      return 'getAll'
   } */

   getAll() {
      return this.productService.getAll()
   }

   /* @Get(':id')
   getOne(@Param() params) {
      return 'getOne' + params.id
   } */

   @Get(':id')
   getOne(@Param('id') id: string) {
      return this.productService.getById(id)
   }

   @Post()
   @HttpCode(HttpStatus.CREATED)
   @Header('Cache-Control', 'none')
   create(@Body() createProductDto: CreateDtoProduct) {
      return this.productService.create(createProductDto)
      /* `Title: ${createProductDto.title}; Price: ${createProductDto.price}` */
   }

   @Put(':id')
   update(@Body() createDtoProduct: CreateDtoProduct, @Param('id') id: string) {
      return 'Update ' + id
   }

   @Delete(':id')
   remove(@Param('id') id: string): string {
      return 'Removed' + id + 'succesfully!'
   }
}
